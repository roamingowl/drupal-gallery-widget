'use strict';
import values from 'lodash.values';

/**
 * @class PlantSort
 *
 */
export default class PlantSort {

    /**
     * @constructor
     * @param {Object} sortSettings
     */
    constructor(sortSettings) {
        this.sortSettings = sortSettings;
    }

    getTitle(plant) {
        return plant.name_lat || '';
    }

    /**
     * returns sorted array
     * @param {Object} plants
     * @returns {Array}
     */
    sortPlants(plants) {
        var sorted = [];

        values(plants).map(function (item) {
            if (this.sortSettings[item.id] && this.sortSettings[item.id].sticky) {
                item.sticky = this.sortSettings[item.id].sticky;
            }
            if (this.sortSettings[item.id] && this.sortSettings[item.id].backgroundColor) {
                item.backgroundColor = this.sortSettings[item.id].backgroundColor;
            }
            if (this.sortSettings[item.id] && this.sortSettings[item.id].previewImages && this.sortSettings[item.id].previewImages.length > 0) {
                item.previewImages = this.sortSettings[item.id].previewImages;
            }
            sorted.push(item);
        }.bind(this));
        sorted.sort((a, b) => {
            if (a.sticky && !b.sticky) {
                return -1
            }
            if (!a.sticky && b.sticky) {
                return 1
            }
            var aName = this.getTitle(a);
            var bName = this.getTitle(b);
            if (aName < bName) {
                return -1;
            }
            if (aName > bName) {
                return 1
            }
            return 0;
        });
        return sorted;
    }
}