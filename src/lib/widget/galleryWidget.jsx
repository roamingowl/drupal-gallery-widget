'use strict';
import React from 'react';
import GalleryTile from './galleryTile.jsx';

export default class GalleryWidget extends React.Component {

    constructor(props) {
        super(props);
        this.webpPostfix = this.props.webp ? '/webp' : '';
    }

    getCardSubtitle(plant) {
        if (plant.name) {
            return plant.name;
        }
        return plant.family_name;
    }

    getCardTitle(plant) {
        if (plant.name_lat) {
            return plant.name_lat;
        }
        return '';
    }

    getTitleStyle(plant) {
        var fontSize = '24px';
        var lineHeight = '1em';
        if (plant.name_lat.length >= 16) {
            fontSize = '20px';
        }
        if (plant.name_lat.length >= 32) {
            fontSize = '16px';
            lineHeight = '1.6em';
        }
        return {
            'font-size': fontSize,
            'font-style': 'italic',
            'line-height': lineHeight,
            textAlign: 'left'
        }
    }

    imageIsLandscape(img) {
        return img.width && img.height && parseInt(img.width) > parseInt(img.height);
    }

    getImagePreviewWidth(img) {
        if (!this.imageIsLandscape(img)) {
            return 250;
        }
        return 512;
    }

    getPlantImage = (plant) => {
        var idx;
        for (idx in plant.images) break;
        if (plant.previewImages && plant.previewImages.length > 0) {
            idx = parseInt(plant.previewImages[0]);
        }
        if (plant.images && plant.images[idx]) {
            var url = plant.images[idx].url + '/max-width/' + this.getImagePreviewWidth(plant.images[idx]) + this.webpPostfix;
            if (url.search('http://') === 0) {
                url = url.replace(/^http\:\/\//, 'https://');
            }
            return url;
        }
        return '';


        if (plant.images && plant.images.length > 0) {
            return plant.images[0].url;
        }
        return null;
    }

    onTileClickHandler = (plant) => {
        window.open(this.props.registryUrl.replace(/\/$/, '') + '/map?plant=' + plant.id, '_blank');
    }

    getTileBackgroundStyle(plant) {
        if (plant.backgroundColor) {
            return {
                backgroundColor: plant.backgroundColor
            }
        }
        return {backgroundColor: '#ffffff'};
    }

    render() {
        var styles = {
            SubtitleStyle: {
                'padding-top': '20px'
            }
        }
        return (
            <div className="tiles-grid">
                {this.props.plants.map((plant) => {
                    return (
                        <GalleryTile imageSrc={this.getPlantImage(plant)}
                                     backgroundStyle={this.getTileBackgroundStyle(plant)}
                                     tileTitleStyle={this.getTitleStyle(plant)}
                                     tileTitle={this.getCardTitle(plant)}
                                     tileSubtitle={this.getCardSubtitle(plant)}
                                     onClick={() => {
                                         this.onTileClickHandler(plant)
                                     }}/>
                    );
                })}
            </div>
        );
    }
}